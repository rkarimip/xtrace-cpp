#include <cstdlib>
#include <ctime>
#include <libconfig.h++>
#include <iostream>
#include "xtrace_sampler.h"

bool XTraceSampler::SamplingSetting::taskSample = true;
double XTraceSampler::SamplingSetting::taskSamplingRate = 1.0;
bool XTraceSampler::SamplingSetting::logSample = true;
double XTraceSampler::SamplingSetting::logSamplingRate = 1.0;

void XTraceSampler::SamplingSetting::getDefaultSetting() {
    libconfig::Config config;
	try {
		config.readFile("../resources/reference.cfg");
		const libconfig::Setting &root = config.getRoot();
		const libconfig::Setting &sampling = root["xtrace"]["sampling"];
		taskSample = sampling["task"].lookup("sample");
		taskSamplingRate = sampling["task"].lookup("sampling_rate");
		logSample = sampling["log"].lookup("sample");
		logSamplingRate = sampling["log"].lookup("sampling_rate");
	}
	catch(const std::exception& e) {
		std::cerr << e.what() << '\n';
	}
}

bool XTraceSampler::SamplingSetting::getTaskSample() {
	return taskSample;
}

double XTraceSampler::SamplingSetting::getTaskSamplingRate() {
	return taskSamplingRate;	
}

bool XTraceSampler::SamplingSetting::getLogSample() {
	return logSample;
}

double XTraceSampler::SamplingSetting::getLogSamplingRate() {
	return logSamplingRate;	
}

XTraceSampler::Sampler::Sampler() {
	this -> count = 0;
}

void XTraceSampler::Sampler::incrCount() {
	this -> count ++;
}

int XTraceSampler::Sampler::getCount() {
	return this -> count;
}

bool XTraceSampler::Sampler::sample() {
	return true;
}

double XTraceSampler::Sampler::getParameter() {
	return 1.0;
}

void XTraceSampler::Sampler::setParameter(double p) {}


XTraceSampler::UniformSampler::UniformSampler(double p, unsigned int seed):Sampler() {
	this -> p = p;
	srand(seed);
}

bool XTraceSampler::UniformSampler::sample() {
    if ((double)rand() / RAND_MAX < this -> p) {
        return true;
    }
    else {
    	return false;
	}
}

void XTraceSampler::UniformSampler::setParameter(double p) {
	this -> p = p;
}

int XTraceSampler::samplingDecisionsNum = 0;
XTraceSampler::Sampler* XTraceSampler::taskSampler = NULL;
XTraceSampler::Sampler* XTraceSampler::logSampler = NULL;

void XTraceSampler::createSampler(SamplingMode mode) {
	SamplingSetting::getDefaultSetting();
	switch (mode)
	{
	case XTraceSampler::TASK:
		taskSampler = getNewSampler(SamplingSetting::getTaskSample(), SamplingSetting::getTaskSamplingRate());
		break;

	case XTraceSampler::LOG:
		logSampler = getNewSampler(SamplingSetting::getLogSample(), SamplingSetting::getLogSamplingRate());
		break;
	}
}

XTraceSampler::Sampler* XTraceSampler::getNewSampler(bool sample, double samplingRate) {
	if (sample) {
		return new UniformSampler(samplingRate, static_cast<unsigned int>(clock()));
	}
	else {
    	return new Sampler();
	}
}

void XTraceSampler::initSampler() {
	taskSampler = NULL;
	logSampler = NULL;
    samplingDecisionsNum = 0;
}

void XTraceSampler::incrSamplingDecisions() {
	samplingDecisionsNum++;
}

XTraceSampler::Sampler** XTraceSampler::getSamplerPointer(SamplingMode mode) {
	switch (mode)
	{
	case XTraceSampler::TASK:
		return &taskSampler;

	case XTraceSampler::LOG:
		return &logSampler;
	}
}

bool XTraceSampler::sample(SamplingMode mode) {
	Sampler** sampler = getSamplerPointer(mode);

	if ((*sampler) == NULL) {
		createSampler(mode);
	}

	incrSamplingDecisions();
	
	if ((*sampler) -> sample()) {
    	(*sampler) -> incrCount();
    	return true;
	}
    else {
    	return false;
	}
}

int XTraceSampler::getSamplingDecisionsNumber() {
	return samplingDecisionsNum;
}