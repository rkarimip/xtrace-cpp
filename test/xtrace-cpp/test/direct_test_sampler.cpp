#include <catch2/catch.hpp>

#include "xtrace_sampler.h"
#include "xtrace.h"
#include "pubsub.h"
#include "xtrace_baggage.h"

TEST_CASE( "Directly test sample tasks = false", "[no-sample]" ) {
    XTraceSampler::initSampler();

    int sampleCallsNumber = 1000;
    XTraceSampler::taskSampler = new XTraceSampler::Sampler();

    for (int i = 0; i < sampleCallsNumber; i++) {
        REQUIRE(XTraceSampler::sample(XTraceSampler::TASK));
    }

    REQUIRE(XTraceSampler::taskSampler -> getCount() == sampleCallsNumber);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == sampleCallsNumber);
}

TEST_CASE( "Directly test sample logs = false", "[no-sample]" ) {
    XTraceSampler::initSampler();

    int sampleCallsNumber = 1000;
    XTraceSampler::logSampler = new XTraceSampler::Sampler();

    for (int i = 0; i < sampleCallsNumber; i++) {
        REQUIRE(XTraceSampler::sample(XTraceSampler::LOG));
    }

    REQUIRE(XTraceSampler::logSampler -> getCount() == sampleCallsNumber);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == sampleCallsNumber);
}

TEST_CASE( "Directly test sample = true, p = 0.5", "[sample]" ) {
    XTraceSampler::initSampler();

    int tracesNumber = 1000;
    double samplingRate = 0.48;
    XTraceSampler::taskSampler = new XTraceSampler::UniformSampler(0.5, 20);

    for (int i = 0; i < tracesNumber; i++) {
        XTraceSampler::sample(XTraceSampler::TASK);
    }

    int decisionsNum = XTraceSampler::getSamplingDecisionsNumber();
    REQUIRE((double)XTraceSampler::taskSampler -> getCount() / decisionsNum == samplingRate);
    REQUIRE(decisionsNum == tracesNumber);
}

TEST_CASE( "Check results for sample = true", "[sample]" ) {
    XTraceSampler::initSampler();

    int tracesNumber = 10;
    XTraceSampler::taskSampler = new XTraceSampler::UniformSampler(0.2, 20);
    bool expectedDecisions[10] = {true, false, false, false, false, false, false, false, false, false};

    for (int i = 0; i < tracesNumber; i++) {
        REQUIRE(XTraceSampler::sample(XTraceSampler::TASK) == expectedDecisions[i]);
    }
}

TEST_CASE( "Check results for sampling modes", "[sample]" ) {
    XTraceSampler::initSampler();

    int tracesNumber = 5;
    int logsNumber = 5;
    double pTask = 0.6;
    double pLog = 0.8;
    bool expectedTasksDecisions[] = { true, false, true, true, false };
    bool expectedLogsDecisions[] = { true, true, false, true, true };
    XTraceSampler::taskSampler = new XTraceSampler::UniformSampler(pTask, 20);
    XTraceSampler::logSampler = new XTraceSampler::UniformSampler(pLog, 30);

    for (int i = 0; i < tracesNumber; i++) {
        REQUIRE(XTraceSampler::sample(XTraceSampler::TASK) == expectedTasksDecisions[i]);
    }
    
    for (int i = 0; i < logsNumber; i++) {
        REQUIRE(XTraceSampler::sample(XTraceSampler::LOG) == expectedLogsDecisions[i]);
    }
}
