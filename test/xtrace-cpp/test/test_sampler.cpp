#include <catch2/catch.hpp>

#include "xtrace_sampler.h"
#include "xtrace.h"
#include "pubsub.h"
#include "xtrace_baggage.h"

TEST_CASE( "Test sample = false", "[no-sample]" ) {
    XTraceBaggage::Clear(); // Clear old X-Trace metadata if it exists
    XTraceSampler::initSampler();

    int tracesNumber = 1000;

    for (int i = 0; i < tracesNumber; i++) {
        uint64_t oldTaskID = XTraceBaggage::GetTaskID();
        XTrace::StartTrace("Testing trace");
        REQUIRE(XTraceBaggage::HasTaskID());
        REQUIRE(XTraceBaggage::GetTaskID() != oldTaskID);
        XTrace::log("Ending trace");
    }

    REQUIRE(XTraceSampler::taskSampler -> getCount() == tracesNumber);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == tracesNumber);
}

TEST_CASE( "Test sample = true, p = 0", "[sample]" ) {
    XTraceBaggage::Clear(); // Clear old X-Trace metadata if it exists
    XTraceSampler::initSampler();

    int tracesNumber = 1000;
    XTraceSampler::taskSampler = new XTraceSampler::UniformSampler(0, static_cast<unsigned int>(clock()));

    for (int i = 0; i < tracesNumber; i++) {
        XTrace::StartTrace("Testing trace");
        REQUIRE(!XTraceBaggage::HasTaskID());
        REQUIRE(!XTraceBaggage::HasParentEventIDs());
    }

    REQUIRE(XTraceSampler::taskSampler -> getCount() == 0);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == tracesNumber);
}

TEST_CASE( "Test sample = true, p = 1", "[sample]" ) {
    XTraceBaggage::Clear(); // Clear old X-Trace metadata if it exists
    XTraceSampler::initSampler();

    int tracesNumber = 1000;
    XTraceSampler::taskSampler = new XTraceSampler::UniformSampler(1, static_cast<unsigned int>(clock()));

    for (int i = 0; i < tracesNumber; i++) {
        XTrace::StartTrace("Testing trace");
        XTrace::log("Ending trace");
        REQUIRE(XTraceBaggage::HasTaskID());
        REQUIRE(XTraceBaggage::HasParentEventIDs());
    }

    REQUIRE(XTraceSampler::taskSampler -> getCount() == tracesNumber);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == tracesNumber);
}

TEST_CASE( "Check has parents for sample = true", "[sample]" ) {
    XTraceBaggage::Clear(); // Clear old X-Trace metadata if it exists
    XTraceSampler::initSampler();

    int tracesNumber = 2;
    bool expectedDecisions[2] = {true, false};
    XTraceSampler::taskSampler = new XTraceSampler::UniformSampler(1, static_cast<unsigned int>(clock()));

    for (int i = 0; i < tracesNumber; i++) {
        XTraceSampler::taskSampler -> setParameter(1 - i);
        XTrace::StartTrace("Testing trace");

        REQUIRE(XTraceBaggage::HasTaskID() == expectedDecisions[i]);
        REQUIRE(XTraceBaggage::HasParentEventIDs() == expectedDecisions[i]);
    }
}

TEST_CASE( "Check no parents for sample = true", "[sample]" ) {
    XTraceBaggage::Clear(); // Clear old X-Trace metadata if it exists
    XTraceSampler::initSampler();

    int tracesNumber = 2;
    bool expectedDecisions[2] = {false, true};
    XTraceSampler::taskSampler = new XTraceSampler::UniformSampler(1, static_cast<unsigned int>(clock()));

    for (int i = 0; i < tracesNumber; i++) {
        XTraceSampler::taskSampler -> setParameter(i);
        XTrace::StartTrace("Testing trace");

        REQUIRE(XTraceBaggage::HasTaskID() == expectedDecisions[i]);
        REQUIRE(XTraceBaggage::HasParentEventIDs() == expectedDecisions[i]);
    }
}

TEST_CASE("No task sampling, log sampling, p = 0.5", "[sample]") {
    XTraceBaggage::Clear();
    XTraceSampler::initSampler();

    int tracesNumber = 100;
    int logsNumber = 20;
    double p = 0.5;
    XTraceSampler::taskSampler = new XTraceSampler::Sampler();
    XTraceSampler::logSampler = new XTraceSampler::UniformSampler(p, 20);

    for (int i = 0; i < tracesNumber; i++) {
        XTrace::StartTrace("Test trace");
        REQUIRE(XTraceBaggage::HasTaskID());

        for (int j = 0; j < logsNumber; j++) {
            XTrace::log("Log No. " + j);
            REQUIRE(XTraceBaggage::HasParentEventIDs());
        }
        
        XTraceBaggage::Clear();
    }

    REQUIRE(XTraceSampler::taskSampler -> getCount() == tracesNumber);
    REQUIRE(XTraceSampler::logSampler -> getCount()  == 0);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == tracesNumber);
}

TEST_CASE("Task sampling, no log sampling, p = 1", "[sample]") {
    XTraceBaggage::Clear();
    XTraceSampler::initSampler();

    int tracesNumber = 100;
    int logsNumber = 20;
    double p = 1.0;
    XTraceSampler::taskSampler = new XTraceSampler::UniformSampler(p, 20);
    XTraceSampler::logSampler = new XTraceSampler::Sampler();

    for (int i = 0; i < tracesNumber; i++) {
        XTrace::StartTrace("Test trace");
        REQUIRE(XTraceBaggage::HasTaskID());

        for (int j = 0; j < logsNumber; j++) {
            XTrace::log("Log No. " + j);
            REQUIRE(XTraceBaggage::HasParentEventIDs());
        }
        
        XTraceBaggage::Clear();
    }

    REQUIRE(XTraceSampler::taskSampler -> getCount() == tracesNumber);
    REQUIRE(XTraceSampler::logSampler -> getCount()  == 0);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == tracesNumber);
}

TEST_CASE("Task sampling, no log sampling, p = 0", "[sample]") {
    XTraceBaggage::Clear();
    XTraceSampler::initSampler();

    int tracesNumber = 100;
    int logsNumber = 20;
    double p = 0.0;
    XTraceSampler::taskSampler = new XTraceSampler::UniformSampler(p, 20);
    XTraceSampler::logSampler = new XTraceSampler::Sampler();

    for (int i = 0; i < tracesNumber; i++) {
        XTrace::StartTrace("Test trace");
        REQUIRE(!XTraceBaggage::HasTaskID());

        for (int j = 0; j < logsNumber; j++) {
            XTrace::log("Log No. " + j);
            REQUIRE(XTraceBaggage::HasTaskID());
            REQUIRE(XTraceBaggage::HasParentEventIDs());
        }
        
        XTraceBaggage::Clear();
    }

    REQUIRE(XTraceSampler::taskSampler -> getCount() == 0);
    REQUIRE(XTraceSampler::logSampler -> getCount()  == tracesNumber);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == tracesNumber * 2);
}

TEST_CASE("Task sampling, no log sampling, p = 0.5", "[sample]") {
    XTraceBaggage::Clear();
    XTraceSampler::initSampler();

    int tracesNumber = 100;
    int logsNumber = 20;
    double p = 0.5;
    int expectedSampledTasks = 55;
    XTraceSampler::taskSampler = new XTraceSampler::UniformSampler(p, 20);
    XTraceSampler::logSampler = new XTraceSampler::Sampler();

    for (int i = 0; i < tracesNumber; i++) {
        XTrace::StartTrace("Test trace");

        for (int j = 0; j < logsNumber; j++) {
            XTrace::log("Log No. " + j);
            REQUIRE(XTraceBaggage::HasTaskID());
            REQUIRE(XTraceBaggage::HasParentEventIDs());
        }
        
        XTraceBaggage::Clear();
    }

    REQUIRE(XTraceSampler::taskSampler -> getCount() + XTraceSampler::logSampler -> getCount() == tracesNumber);
    REQUIRE(XTraceSampler::taskSampler -> getCount()  == expectedSampledTasks);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == tracesNumber * 2 - expectedSampledTasks);
}

TEST_CASE("Task sampling, log sampling, pTask = 0.5, pLog = 0", "[sample]") {
    XTraceBaggage::Clear();
    XTraceSampler::initSampler();

    int tracesNumber = 100;
    int logsNumber = 20;
    double pTask = 0.5;
    double pLog = 0.0;
    int expectedSampledTasks = 46;
    XTraceSampler::taskSampler = new XTraceSampler::UniformSampler(pTask, 20);
    XTraceSampler::logSampler = new XTraceSampler::UniformSampler(pLog, 20);

    for (int i = 0; i < tracesNumber; i++) {
        XTrace::StartTrace("Test trace");
        bool isSampled = XTraceBaggage::HasTaskID();

        for (int j = 0; j < logsNumber; j++) {
            XTrace::log("Log No. " + j);
            REQUIRE(isSampled == XTraceBaggage::HasTaskID());
            REQUIRE(isSampled == XTraceBaggage::HasParentEventIDs());
        }
        
        XTraceBaggage::Clear();
    }

    REQUIRE(XTraceSampler::logSampler -> getCount() == 0);
    REQUIRE(XTraceSampler::taskSampler -> getCount()  == expectedSampledTasks);
    REQUIRE(XTraceSampler::getSamplingDecisionsNumber() == tracesNumber + ((tracesNumber - expectedSampledTasks) * logsNumber));
}

TEST_CASE("Different samplers, pTask = 0.5, pLog = 0.1", "[sample]") {
    XTraceBaggage::Clear();
    XTraceSampler::initSampler();

    int tracesNumber = 100;
    int logsNumber = 20;
    double pTask = 0.5;
    double pLog = 0.1;
    int expectedSampledTasks = 53;
    int expectedSampledLogs = 40;
    XTraceSampler::taskSampler = new XTraceSampler::UniformSampler(pTask, 20);
    XTraceSampler::logSampler = new XTraceSampler::UniformSampler(pLog, 30);

    for (int i = 0; i < tracesNumber; i++) {
        XTrace::StartTrace("Test trace");

        for (int j = 0; j < logsNumber; j++) {
            XTrace::log("Log No. " + j);
        }
        
        XTraceBaggage::Clear();
    }

    REQUIRE(XTraceSampler::logSampler -> getCount() == expectedSampledLogs);
    REQUIRE(XTraceSampler::taskSampler -> getCount()  == expectedSampledTasks);
}

TEST_CASE("Same samplers, p = 0.2", "[sample]") {
    XTraceBaggage::Clear();
    XTraceSampler::initSampler();

    int tracesNumber = 100;
    int logsNumber = 20;
    double p = 0.2;
    int expectedSampledTasks = 17;
    int expectedSampledLogs = 79;
    XTraceSampler::taskSampler = new XTraceSampler::UniformSampler(p, 20);
    XTraceSampler::logSampler = new XTraceSampler::UniformSampler(p, 20);

    for (int i = 0; i < tracesNumber; i++) {
        XTrace::StartTrace("Test trace");

        for (int j = 0; j < logsNumber; j++) {
            XTrace::log("Log No. " + j);
        }
        
        XTraceBaggage::Clear();
    }

    REQUIRE(XTraceSampler::logSampler -> getCount() == expectedSampledLogs);
    REQUIRE(XTraceSampler::taskSampler -> getCount()  == expectedSampledTasks);
    
    PubSub::shutdown();
    PubSub::join();
}
