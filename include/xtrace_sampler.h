#ifndef _XTRACE_SAMPLER_
#define _XTRACE_SAMPLER_

namespace XTraceSampler {

	namespace SamplingSetting {
		extern bool taskSample;
		extern double taskSamplingRate;
		extern bool logSample;
		extern double logSamplingRate;

		void getDefaultSetting();
		bool getTaskSample();
		double getTaskSamplingRate();
		bool getLogSample();
		double getLogSamplingRate();
	}


	class Sampler {
		private:
		int count;

		public:
		Sampler();
		void incrCount();
		int getCount();
		virtual bool sample();
		virtual double getParameter();
		virtual void setParameter(double);
	};


	class UniformSampler: public Sampler {	
		private:
		double p;
			
		public:
		UniformSampler(double p, unsigned int seed);
		bool sample();
		void setParameter(double p);
	};

	enum SamplingMode {TASK, LOG};
	
	extern int samplingDecisionsNum;
	extern Sampler* taskSampler;
	extern Sampler* logSampler;

	void createSampler(SamplingMode mode);
	Sampler* getNewSampler(bool sample, double samplingRate);
	void initSampler();
	void incrSamplingDecisions();
	Sampler** getSamplerPointer(SamplingMode mode);
	bool sample(SamplingMode mode);
	int getSamplingDecisionsNumber();
}

#endif
